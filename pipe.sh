#!/usr/bin/env bash
#
# Run a command or script on your server
#
# Required globals:
#   SSH_USER
#   SERVER
#   GIT_BRANCH
#   GIT_REMOTE_PATH
#
# Optional globals:
#   TEAMS_WEBHOOK (default: null)
#   POST_INSTALL (default: null)
#   EXTRA_ARGS
#   ENV_VARS
#   DEBUG (default: "false")
#   SSH_KEY (default: null)
#   PORT (default: 22)
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

validate() {
  # required parameters
  : SSH_USER=${SSH_USER:?'SSH_USER variable missing.'}
  : SERVER=${SERVER:?'SERVER variable missing.'}
  : GIT_BRANCH=${GIT_BRANCH:?'GIT_BRANCH variable missing.'}
  : GIT_REMOTE_PATH=${GIT_REMOTE_PATH:?'GIT_REMOTE_PATH variable missing.'}
  : DEBUG=${DEBUG:="false"}
}

setup_ssh_dir() {
  INJECTED_SSH_CONFIG_DIR="/opt/atlassian/pipelines/agent/ssh"
  # The default ssh key with open perms readable by alt uids
  IDENTITY_FILE="${INJECTED_SSH_CONFIG_DIR}/id_rsa_tmp"
  # The default known_hosts file
  KNOWN_SERVERS_FILE="${INJECTED_SSH_CONFIG_DIR}/known_hosts"

  mkdir -p ~/.ssh || debug "adding ssh keys to existing ~/.ssh"
  touch ~/.ssh/authorized_keys

  # If given, use SSH_KEY, otherwise check if the default is configured and use it
  if [ -n "${SSH_KEY}" ]; then
     info "Using passed SSH_KEY"
     (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/pipelines_id)
  elif [ ! -f ${IDENTITY_FILE} ]; then
     fail "No default SSH key configured in Pipelines."
  else
     info "Using default ssh key"
     cp ${IDENTITY_FILE} ~/.ssh/pipelines_id
  fi

  if [ ! -f ${KNOWN_SERVERS_FILE} ]; then
      fail "No SSH known_hosts configured in Pipelines."
  fi

  cat ${KNOWN_SERVERS_FILE} >> ~/.ssh/known_hosts

  if [ -f ~/.ssh/config ]; then
      debug "Appending to existing ~/.ssh/config file"
  fi
  echo "IdentityFile ~/.ssh/pipelines_id" >> ~/.ssh/config
  chmod -R go-rwx ~/.ssh/
}

git_checkout_and_pull() {
  info "Executing ${MODE} on ${SERVER}"

  # Format the deployment command
  COMMAND="cd ${GIT_REMOTE_PATH} && git fetch && git checkout ${GIT_BRANCH} && git pull"

  run ssh -A -tt -i ~/.ssh/pipelines_id -o 'StrictHostKeyChecking=no' -p ${PORT:-22} ${EXTRA_ARGS} $SSH_USER@$SERVER ${ENV_VARS} "bash -c '$COMMAND'"

  # If we have a post install script, execute after deployment
  if [[ -n ${POST_INSTALL} ]]; then
    info "Executing script ${COMMAND} on ${SERVER}"
    run ssh -i ~/.ssh/pipelines_id -o 'StrictHostKeyChecking=no' -p ${PORT:-22} ${EXTRA_ARGS} $SSH_USER@$SERVER ${ENV_VARS} 'bash -s' < "$POST_INSTALL"
  fi
}

send_notification_to_teams() {
  if [[ -n ${TEAMS_WEBHOOK} ]]; then
    info "Send Teams notification"

    if [[ $1 == "ok" ]]; then
      # No errors
      debug "TODO: send 'ok' message."
    else
      # An error occurred
      debug "TODO: send 'error' message."
    fi
  fi
}

run_pipe() {
  git_checkout_and_pull

  if [[ "${status}" == "0" ]]; then
    success "Execution finished."
    # send_notification_to_teams "ok"
  else
    fail "Execution failed."
    # send_notification_to_teams "error"
  fi
}

validate
setup_ssh_dir
run_pipe